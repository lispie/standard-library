(include "standard-library/boolean.lispie")

(define function (macro arguments
    "a macro that allows to define functions in a shorter way"
    (if (lt (length arguments) 2) (error (concat "too few arguments provided for macro 'function' (expected at least 2 but got " (concat (show (length arguments)) ")"))) ())
    (if (not (is-symbol (head arguments))) (error "illegal argument provided for macro 'function' (expected a symbol as 1st argument)") ())
    (define fn (cons (quote lambda) (cons (head (tail arguments)) (tail (tail arguments)))))
    (cons (quote define) (cons (head arguments) (cons fn ())))))

(function id (x)
    "the identity function which simply returns its argument"
    x)

(function call (f arguments)
    "calls function 'f' with the provided list of arguments"

    (define escape (lambda (xs)
        (if (equals xs ())
            ()
            (cons (cons (quote quote) (cons (head xs) ())) (escape (tail xs)))
        )))

    (eval (cons f (escape arguments))))

(function compose (g f)
    "composes g and f by returning a lambda which first applies f to the list
     of arguments and then applies g to f's scalar result"
    (lambda arguments
        (call g (cons (call f arguments) ()))))

(function andThen (f g)
    "composes g and f by returning a lambda which first applies f to the list
     of arguments and then applies g to f's scalar result"
    (compose g f))

