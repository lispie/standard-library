# Standard Library for `lispie`

This repository contains the standard library for the programming language `lispie`.

In order to get this library to work it is substantial to load the following prelude into the global context (also available as a separate [file](prelude.lispie)). 
This prelude references built-in definitions only and contributes two crucial definitions, namely `sequence` and `include`.
After loading it into the global context it is possible to include arbitrary `*.lispie` files.

```lisp
(define sequence ((lambda () "memoization scope"
    (define if (macro (condition ifTrue ifFalse)
        "a temporary if definition since boolean operations may not be loaded yet"
        (choose (eval condition) ifTrue ifFalse)))

    (define return (lambda (result next)
        (if (equals next ())
            result
            (return (head next) (tail next)))))

    (lambda arguments
        (if (equals arguments ())
            ()
            (return (head arguments) (tail arguments)))))))

(define scope (macro arguments
    (cons (cons (quote lambda) (cons (quote ()) arguments)) ())))

(define include (macro (path)
    (cons (quote sequence) (parse (slurp (eval path))))))
```

#### FAQ

- *Question*: What should I do when encountering a bug?
   
    *Answer*: Please open up a new issue in gitlab!
- *Question*: Well, is it likely to encounter a bug?
    
    *Answer*: I have to admit: Most likely. 
    I didn't have the time to test the code properly. 
    Yet I tried out most of the functionality on my local machine and everything worked fine. 
- *Question*: May I contribute code?

    *Answer*: Yes, you're welcome! 
    However, please make sure to properly use the issue system and an own branch. 
    You can merge changes and features by creating a merge request to the development branch!
    Needless to say that I will review your changes, isn't it? :-)
 